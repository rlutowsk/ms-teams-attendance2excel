# MS Teams Attendance2Excel

## Information

Small utility that converts meetingAttendanceList*.csv file from MS Teams meeting and creates a clipboard content, also in csv format. The output is ready to be pasted in Excel for example and consists of the following columns:
- Name of a student
- Date of the meeting
- Total time of participation - calculated up to certain time, defined in the script

## Instruction

In the most basic scenario one should verify and probably change the default dictionary, which should be quite self-explanatory. This should be followed by just running the script and at last - paste the clipboard content into a spreadsheet.

## Copyrights?

The ideas and probably larger portion of the code is taken from the website

https://stackoverflow.com/questions/63231905/how-to-consolidate-the-attendance-list-from-ms-teams

All credits should go to the authors of codes in the above webpage.

If in any manner any author legal rights are violated, please inform the author and the project will be shut down.
