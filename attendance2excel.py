import pandas as pd
import datetime as dt
import os

default = {
    'organizator': {'desc': 'Imię i nazwisko organizatora', 'val': 'Rafał Lutowski'},
    'data': {'desc': 'Data spotkania', 'val': dt.date.today().strftime("%Y-%m-%d")},
    'zakonczenie': {'desc': 'Godzina zakończenia spotkania', 'val': '09:55'},
    'nazwa_pliku': {'desc': 'Nazwa pliku z danymi', 'val': 'meetingAttendanceList.csv'}
}
def input_str(desc, val):
    r = input(f'{desc} [{val}]: ')
    if r == '':
        r = val
    return r

def input_file(desc, val):
    r = input(f'{desc} [{val}]: ')
    if r == '':
        return val
    if r[0] == '+':
        n = int(r)
        f = os.path.splitext(val)
        return f'{f[0]} ({n}){f[1]}'
    return r

handles = {
    'organizator': input_str,
    'data': input_str,
    'zakonczenie': input_str,
    'nazwa_pliku': input_file
}

config = {}

def fill_missing_dates(join_date, left_date, replace_dates):
    if left_date is not pd.NaT:
        return left_date
    for date in replace_dates:
        if join_date < date:
            return date
    return replace_dates[-1]

for key in default:
    func = handles[key]
    config[key] = func(default[key]["desc"], default[key]["val"])

organizer_name = config['organizator']
date           = config['data']
end_timestamp  = pd.Timestamp(config['zakonczenie'])
filename       = config['nazwa_pliku']

df = pd.read_csv(filename, delimiter='\t', encoding='UTF-16LE')

df.rename(columns={'Znacznik czasu':'timestamp', 'Akcja użytkownika' : 'Akcja'}, inplace=True)

# Pivot User Action values to columns; we don't need 'Join before'
df['timestamp'] = pd.to_datetime(df['timestamp'])
df['Akcja'] = df['Akcja'].str.replace('Dołączył przed', 'Dołączył')
df = df.set_index(['Imię i nazwisko', 'Akcja'], append=True).unstack()
df.columns = df.columns.get_level_values(1)

# we can (always) shift the 'Left' dates due to underlying data structure
df['Opuścił(a)'] = df['Opuścił(a)'].shift(-1)
df = df.dropna(how='all')

# organizer can only have one missing value: the end value
mask_organizer = df.index.get_level_values('Imię i nazwisko') == organizer_name
df.loc[mask_organizer, 'Opuścił(a)'] = df.loc[mask_organizer, 'Opuścił(a)'].fillna(end_timestamp)
replace_na_dates = list(df.loc[mask_organizer, 'Opuścił(a)'])

df['Opuścił(a)'] = df.apply(lambda x: fill_missing_dates(x['Dołączył'], x['Opuścił(a)'], replace_na_dates), axis=1)
df['Obecność'] = df['Opuścił(a)'] - df['Dołączył']
df['Data'] = df.apply(lambda x: x['Dołączył'].strftime('%Y-%m-%d'), axis=1)

df=df.groupby(['Imię i nazwisko','Data']).sum()
df['Obecność']=df.apply(lambda x: x['Obecność'].total_seconds(), axis=1)
df.sort_values('Imię i nazwisko', inplace=True)
df.reset_index(level=['Imię i nazwisko','Data'], inplace=True)

df = df[['Imię i nazwisko','Data','Obecność']]

df.to_clipboard(index=False, header=False, decimal=',')

#print(df.head())
